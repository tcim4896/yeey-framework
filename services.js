var services = {
	_menu:{
		state: {
			structure:{},
			opened:0,
			offset:px(40),
			size:px(200),
		},
		init: function(providers){
			structure.map.levels.map(function(level){
				level.size="0px";
				return level;
			});
		},
		opened:[],
		offset:30,
		size:200,

		isOverlapped: function isOverlapped(id) {
    		return this.opened.indexOf(id) !== (this.opened.length-1);
		},
		getList: function getList(id, structure){
			return structure.lists[id];
		},
		action: function action(type, path){
			var self = this;
			var levels = self.structure.levels;

			switch(type){
				case "toggle":
					if(self.opened.indexOf(path) === -1){
						//push one level id to opened if not opened
						self.opened.push(path);
					}else{
						path === 0 ? this.opened = [] : {}
					}
				adaptSize();
				break;
				case "closeUntil":
					closeUntil(path);
					adaptSize();
				break;
				case "link":
					self.opened = [];
					adaptSize();
					//_router.navigateByUrl(path) 
				break;
			}
		},
	    closeUntil: function closeUntil(id){
	      self.opened = self.opened.slice(0, self.opened.indexOf(id)+1);
		},
	    adaptSize: function adaptSize(){
			var sizes = [];
			levels.map(function(level){
				!self.opened.includes(level.id) ?
				level.size = "0px" : {}
				return level;
			});
			sizes = self.opened.map(function(level, index){
				return self.px(self.size + (index * self.offset));
			}).reverse();
			self.opened.map(function(level, index){
				levels[level].size = sizes[index];
			});
	    }
	},
	_router:{
		state: {}
	}
};