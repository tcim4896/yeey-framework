let body=document.body;
let cl=console.log;
let px = (value) => typeof value === "string" ? parseInt(value) : value + "px";
let pxSeq = (seq) => seq.map(val=>px(val)).reduce((a,b)=>a+b,0);
let getStyle = (elm) => elm.style;