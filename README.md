# yeey framework #
a lightweight javascript framework.

## notes
ar1=[1,2,3];
arCopy1=ar1.slice(0);
this is an assign copy of ar1 to arCopy1

but what about object references within the array?
ar1=[1,2,{a:1}];
arCopy1=ar1.slice(0);

a={a:1}
{a: 1}
ar1=[a,1,2]
(3) [{…}, 1, 2]0: {a: 1}1: 12: 2length: 3__proto__: Array(0)
ar2=[a,3,3]
(3) [{…}, 3, 3]0: {a: 1}1: 32: 3length: 3__proto__: Array(0)
ar2[0].a=2
2
ar1
(3) [{…}, 1, 2]0: {a: 2}a: 2__proto__: Object1: 12: 2length: 3__proto__: Array(0)


	// if none is object; iterationCopy will do , but
	// if we have an array we'd map and check if value = obj
	// then we'd do the iterationCopy if none is object
	// what if one is an object?
	// ..
	// using target[prop]=src[prop]
	// the problem is [1,2,{a:1}]
	// if array then array.map if val = obj use fullCopy
	// what will fullCopy return?
	// else just return the val