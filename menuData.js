var menuData = {
 levels: [
   { text: "Home", id: 0, listId: 0 },
   { text: "Music", id: 1, listId: 1 },
   { text: "Software", id: 2, listId: 2 },
 ],
 lists: [
 {
  id: 0,
  items: [
   { text: "Music", type: "toggle", path: 1 },
  ]
 },
 {
  id: 1,
  items: [
   { text: "Drum and Bass", type: "link", path: "music/drum-and-bass" },
   { text: "Hardcore", type: "link", path: "music/hardcore" },
   { text: "Psytrance", type: "link", path: "music/psytrance" },
  ]
 }],
};